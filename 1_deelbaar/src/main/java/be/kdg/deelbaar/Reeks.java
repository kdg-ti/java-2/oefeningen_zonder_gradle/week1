package be.kdg.deelbaar;

/*
    TODO: De klasse Reeks implementeert de interface Deelbaar.
    Als je de reeks deelt dan geef je de eerste helft van de reeks terug
 */
public class Reeks {
    private int[] array;

    public Reeks(int[] array) {
        this.array = array;
    }

    public String toString() {
        StringBuilder str = new StringBuilder("[");
        for (int i = 0; i < array.length; i++) {
            str.append(array[i]);
            if (i < array.length - 1) str.append(", ");
        }
        return str + "]";
    }
}
