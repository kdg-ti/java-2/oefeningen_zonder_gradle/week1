package be.kdg.interfacedemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Vul aan waar nodig. Maak gebruik van de Sorteerder interface.
 */
public class Woorden {
    private List woorden;

    /* Maak hier de ArrayList */
    public Woorden(List woorden) {

    }

    /* Kopieer hier de tabel van Strings in de ArrayList */
    public Woorden(String[] woorden) {

    }

    /* Sorteer met de sort methode uit de Collections klasse */
    public void sort() {

    }

    /* Sorteer met de sort methode uit de Collections klasse
     * Gebruik als tweede parameter een specifieke methode,
     * Zie hiervoor de klasse Collections
     */
    public void sortReversed() {

    }

    /* Zorg ervoor dat de gewenste afdruk op het scherm komt */
    public String toString() {
        return "nonsens";
    }
}
