package be.kdg.copy.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Werk de nodige methoden verder uit.
 */
public class Personen {
    private final List<Persoon> personen;

    public Personen() {
        personen = new ArrayList<>();
    }

    public void voegPersoonToe(Persoon persoon) {

    }

    public String maakPersonenString() {
        return null;
    }

    /*
     * De kopie wordt hier gemaakt, kopieer de volledige collectie met één operatie
     */
    public Personen maakKopie() {
        return null;
    }
}
