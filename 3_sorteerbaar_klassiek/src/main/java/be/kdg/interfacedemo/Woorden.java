package be.kdg.interfacedemo;

/**
 * Vul aan waar nodig. Maak gebruik van de Sorteerder interface.
 */
public class Woorden {
    private Woord woorden[];

    /*
     * Maak hier de nieuwe tabel. Kopieer dan de tabel van strings
     * naar de nieuwe tabel van het type Woord. Maak gebruik van new Woord(...)
     * om de strings naar type Woord om te zetten.
     */
    public Woorden(String[] woorden) {

    }

    /* Sorteer met de sort methode (zie klasse BubbleSort) */
    public void sort() {

    }

    /* Sorteer met de sortReversed methode (zie klasse BubbleSort) */
    public void sortReversed() {

    }

    /* Zorg ervoor dat de gewenste afdruk op het scherm komt */
    public String toString() {
        return "nonsens";
    }
}
